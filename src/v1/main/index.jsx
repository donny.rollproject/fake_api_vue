import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import App from './App';
import InitRouteApi from './middleware/InitRouteApi';
import InitPubSub from './middleware/InitPubSub';
import InitLocale from './middleware/InitLocale';
import Middleware from '../../../classes/Middleware';
import DefaultLoading from './loading/DefaultLoading.jsx';
import InitGetUser from './middleware/InitGetUser';
import VModal from 'vue-js-modal';
import Fragment from 'vue-fragment'

/* Common import  */
import "nprogress/nprogress.css";
import "@asset/v1/css/backend.scss";
import "@asset/semantic/dist/semantic.min.js";
import "../../../base/BaseCommon.js";
import "../../../base/Swal.js";

/* If BaseVue not effect by this setup try on to BaseVue file directly like vue-super */
Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(Fragment.Plugin);
Vue.use(VModal, { componentName: 'v-modal' });

let routes = (function(routes){
	require('./auth/Router')(routes);
	require('./activity/Router')(routes);
  require('./dashboard/Route')(routes);
	require('./user/Router')(routes);
  return routes;
})([]);

const router = new VueRouter({
	mode: 'history',
	base: '/',
	routes,
});

var beforeEach = Middleware.bind(Middleware, [
	InitLocale,
	InitPubSub,
	InitRouteApi,
	InitGetUser,
	(to, from, done, nextMiddleware) => {
		return nextMiddleware();
	},
]);
/* Only one called when load first time */
var afterEach = Middleware.bind(Middleware, [
	(to, from) => {
		
	},
]);

router.beforeEach(beforeEach);
router.afterEach(afterEach);

let app = new Vue({
	data: {
		waiting_api_routes : false,
	},
	router,
	watch: {},
	beforeMount() {
		window.router = this.$router;
	},
	mounted() {},
	methods: {
    getApiRoutes : function(){
      let self = this;
      /* Get api route from laravel */
    }
	},
	render(h) {
		if (this.$data.waiting_api_routes  == true) {
			return <DefaultLoading/>;
		}
		return <App/>;
	},
});

app.$mount('#main');