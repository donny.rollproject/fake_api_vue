import BaseVue from 'BaseVue';
import NavigationStore from '../store/NavigationStore';

const SideMenu = BaseVue.extend({
  data : function(){
    return {
      is_active: false,
      current_menu: '',
      current_url : null
    }
  },
  methods : {
    onrouteupdate : function(to,from){
      let self = this;
      self.checkRoute();
    },
    onconfig : function(){
      let self = this;
      NavigationStore.watch((state)=>state.select_url,function(val){
        self.set('current_url',val);
      })
    },
    oncomplete : function(){
      let self = this;
      var segments = window.location.pathname.split( '/' );
      console.log(segments);
      let listUrl = $('.base_list.menu').find('a');
      let isIndex = true;
      listUrl.each(function(index,dom){
        console.log(index,segments[segments.length - 1]);
        switch(segments[segments.length - 1]){
          case undefined:
          case "":
            $(dom).parent().addClass('active');
            isIndex = false;
          return false;
          default:
            if($(dom).attr('href').includes(segments[segments.length - 1]) ){
              // $(dom).parent().addClass('active');
              // $(dom).parent().closest('.submenu').css('display', 'block').siblings('.has_submenu').find('.icon.angle').addClass('down');
              isIndex = false;
            } 
          break;
        }
      })
      if(isIndex){
        // $(listUrl[0]).parent().addClass('active');
      }
      $('.ui.dropdown').dropdown();
      $('.has_submenu').on('click', function(){
        let _this = $(this);
        let submenu = $(this).siblings('.submenu').css('display');

        _this.closest('li').siblings().find('.submenu').slideUp('slow').siblings().find('.icon.angle').removeClass('down');

        if( submenu == 'none' ){
          _this.find('.icon.angle').addClass('down');
        } else{
          _this.find('.icon.angle').removeClass('down');
        }
        $(this).siblings('.submenu').slideToggle("slow");
      });
      
      let sideMenuA = $('#side-menu').find('a');
      let lastFoundA = null;
      for(var a=0;a<sideMenuA.length;a++){
        if(window.location.pathname == $(sideMenuA[a]).attr('href')){
          $(sideMenuA[a]).addClass('active');
          lastFoundA = sideMenuA[a];
        }
      }
      if(lastFoundA != null){
        let hasSubMenu = $(lastFoundA).parents('ul').siblings('.has_submenu');
        hasSubMenu.trigger('click');
      }
    },
    checkRoute : function(){
      let self = this;
      /* Listen url are changed */
      setTimeout(function(){
        let sideMenuA = $('#side-menu').find('a');
        let isFoundUrl = false;
        let select_url = window.location.pathname;
        $('#side-menu').find('a').removeClass('active');
        for(var a=0;a<sideMenuA.length;a++){
          if(select_url == $(sideMenuA[a]).attr('href')){
            $(sideMenuA[a]).addClass('active');
            self.set('current_url',window.location.pathname);
            isFoundUrl = true;
          }
        }
        if(isFoundUrl == false){
          select_url = self.get('current_url');
          for(var a=0;a<sideMenuA.length;a++){
            if(select_url == $(sideMenuA[a]).attr('href')){
              $(sideMenuA[a]).addClass('active');
            }
          }
        }
        // self.recursiveCheckUrl(window.location.pathname.split('/'));
      },1000)
    },
    recursiveCheckUrl : function(pathUrlArray){
      let self = this;
      window.staticType(pathUrlArray,[Array]);
      try{
        console.log('pathUrlArray',pathUrlArray);
        let toPath = pathUrlArray.join('/');
        let sideMenuA = $('#side-menu').find('a');
        let isFoundUrl = false;
        $('#side-menu').find('a').removeClass('active');
        for(var a=0;a<sideMenuA.length;a++){
          console.log(window.location.pathname,$(sideMenuA[a]).attr('href'))
          let aToArray = $(sideMenuA[a]).attr('href').split('/');
          let aToPath = aToArray.slice(aToArray.length - pathUrlArray.length,)
          if(window.location.pathname == $(sideMenuA[a]).attr('href')){
            $(sideMenuA[a]).addClass('active');
            self.set('current_url',window.location.pathname);
          }
        }
      }catch(ex){
        console.error('recursiveCheckUrl - ex ',ex);
      }

    }
  },
  render(){
    return (
      <div id="sidemenu">
        <div class="ui visible sidebar inverted vertical menu" id="wr-sidebar">
          <div class="base_wr column sidemenu" id="side-menu">
            <div style="text-align: center; padding-top: 10px; padding-bottom: 10px;">
                <a href="/v1/dashboard">
                    <img src="/public/img/favicon.png" alt="" style="width: 128px;"/>
                </a>
            </div>
            <ul class="base_list menu">
              <li class="mobile only">
                  <h3 class="base_wr row item">
                      {gettext("Artywiz - Administration")}
                  </h3>
              </li>
              {/* <li>
                <router-link class="base_wr row item has_submenu" to={{name : 'dashboard'}}>
                    <img src="/public/img/sidemenu/commandess.png" alt=""/>
                    <span class="base_info medium bold">{gettext("Dashboard")}</span>
                </router-link>
              </li> */}
              <li>
                <router-link class="base_wr row item has_submenu" to={{name : 'activity.activity'}}>
                    <img src="/public/img/sidemenu/commandess.png" alt=""/>
                    <span class="base_info medium bold">{gettext("Activities")}</span>
                    {/* <i class="icon angle right"></i> */}
                </router-link>
                {/* <ul class="base_list submenu">
                    <li>
                        <router-link class="base_wr row item" to={{name : 'dashboard'}}>
                            <span class="base_info medium bold">{gettext("Liste De Commandes")}</span>
                        </router-link>
                    </li>
                </ul> */}
              </li>
              {/* <li>
                <router-link class="base_wr row item has_submenu" to={{name : 'order.orders'}}>
                    <img src="/public/img/sidemenu/commandess.png" alt=""/>
                    <span class="base_info medium bold">{gettext("Orders")}</span>
                </router-link>
              </li> */}
            </ul>
          </div>
        </div>
      </div>
    )
  }
})

export default SideMenu;