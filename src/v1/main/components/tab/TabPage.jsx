import BaseVue from 'BaseVue';

const TabPage = BaseVue.extend({
  props : {
    setRef : [String]
  },
  data : function(){
    return {
        target : null
    }
  },
  methods : {
    onconfig : function(){
      let self = this;
      window.pubsub.on('element.tab.tab_page',self.responseAction.bind(self));
    },
    responseAction : function(props){
        let self = this;
        self.set('target',props.target);
    }
  },
  render(){
    let { setRef } = this.$props;
    let { target } = this.get();
    if(target == setRef){
      return this.$scopedSlots.default()
    }
    return null;
  }
})

export default TabPage;