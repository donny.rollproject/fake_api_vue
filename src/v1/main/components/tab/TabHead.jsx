import BaseVue from 'BaseVue';

const TabHead = BaseVue.extend({
  props : {
    setTabs : {
      type : [Array],
      default : []
    },
    setActive : {
      type : [Number],
      default : -1
    } 
  },
  data : function(){
    return {
      active : -1,
      tabs : []
    }
  },
  watch : {
    active : async function(val,oldVal){
      let self = this;
      try{
        if(self.getOnListener == null){
            throw 'setOnClickListener should defined!';
        }
        await self.waitingTimeout(100);
        self.getOnListener(val);
      }catch(ex){
        console.error(ex);
      }
    }
  },
  methods : {
    onconfig : function(){
      let self = this;
      self.set('tabs',self.$props.setTabs);
      self.setTabActive(self.$props.setActive);
    },
    handleClick : async function(action,props,e){
      let self = this;
      if(self.get('tabs')[props.index] == null){
        alert('Need set target on tab page!');
        return;
      }
      let target = self.get('tabs')[props.index].key;
      switch(action){
        case 'CLICK':
          await self.set('active',props.index);
          window.pubsub.emit('element.tab.tab_page',{
              target : target,
              index : props.index
          })
        break;
      }
    },
    setOnClickListener : function(func){
      let self = this;
      try{
          if(func == null){
              throw 'setOnClickListener should be defined';
          }
          self.getOnListener = func;
      }catch(ex){
          console.error(ex);
      }
    },
    setTabActive : function(index){
      let self = this;
      self.handleClick('CLICK',{index : index},null);
    },
  },
  render(){
    let { tabs,active } = this.get();
    return (
      <div class="ui top attached tabular menu">
      {tabs.map((value,index)=>{
        return <div class={(parseInt(active)==index?'active':'')+" item"} onClick={this.handleClick.bind(this,'CLICK',{index : index})}>{value.label}</div>
      })}
      </div>
    )
  }
})

export default TabHead;