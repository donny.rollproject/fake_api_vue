import BaseVue from 'BaseVue';
import SimplePagination from './pagination/SimplePagination';
import MenuFilter from './MenuFilter';

const DataList = BaseVue.extend({
  components : {
    'menu-filter' : MenuFilter,
    'simple-pagination' : SimplePagination
  },
  data : function(){
    return {
      datas : [],
      query : {
        page : 1,
        take : 20
      },
    }
  },
  methods : {
    oncomplete : function(){
      this.setInitDOMSelection('pagination');
      this.setInitDOMSelection('menu_filter');
    },
    getDatas : function(){
      return {
        return : []
      }
    },
    setDatas : function(props){
      let self = this;
      window.staticType(props,[null,Object])
      if(props == null) return;
      let datas = (function(datas){
        /* Modif at here */
        return  datas;
      })(props.return);
      self.set('datas',datas);
    },
    setInitDOMSelection : function(action,props){
      let self = this;
      switch(action){
        case 'pagination':
          window.staticType(props,[null,Object]);
          self.pagination = self.$refs.pagination;
          if(self.pagination == null) return;
          self.pagination.setOnChangeListener(async function(action,props){
            self.set('query',{
              ...props
            })
            self.setDatas(await self.getDatas());
          })
          if(props == null) return;
          self.pagination.setPagination({
            take : props.take,
            page : props.page,
            total : props.total
          })
          break;
        case 'menu_filter':
          self.menu_filter = self.$refs.menu_filter;
          if(self.menu_filter == null) return;
      }
    },
  },
  computed : {},
  render : function(h){
    return (
      <div id="users" class="base_wr column" style="position: relative;">
        <menu-filter ref="menu_filter"></menu-filter>
        <table class="ui striped table">
          <thead>
            <tr>
              <th>{ gettext("No") }</th>
              <th>{ gettext("Nom du droit d'accès") }</th>
              <th>{ gettext("Description") }</th>
              <th>{ gettext("Dernière modification") }</th>
              <th>{ gettext("Option") }</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td>
                <div class="list-menu-click" style="position: relative;">
                  <i class="ion ion-md-create"></i>
                </div>
                <div id="popup" class="ui fluid popup bottom left transition hidden" style="">
                  <div class="ui four column relaxed divided grid">
                    <div class="column">
                      <div class="ui link list">
                        <a class="item nowrap" href="{{@this.setUrl(@global.HTTP_REQUEST.PRODUCT.VIEW,[{':id':id}])}}">{gettext("Éditer")}</a>
                        <a class="item nowrap" href="#" on-click="@this.handleClick('DELETE_ITEM',{id : id},@event)">{gettext("Supprimer")}</a>
                      </div>
                    </div>
                  </div>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
        <simple-pagination ref="pagination"></simple-pagination>
      </div>
    )
  }
})

export default DataList;