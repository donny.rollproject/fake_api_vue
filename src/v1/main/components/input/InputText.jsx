import BaseVue from 'BaseVue';

const InputText = BaseVue.extend({
  props : {
    input : [null,Function],
    inputObject : [null,Function],
    label : [String],
    form_title : [String],
    placeholder : [String],
    value : [String],
    disabled : {
      type : [null,Boolean],
      default : false
    },
    readonly : {
      type : [null,Boolean],
      default : false
    },
    name : [String],
    type : [String]
  },
  data : function(){
    return {
      form_data : {}
    }
  },
  methods : {
    oncomplete : function(){
      this.setUpdate('form_data',{
        value : this.$props.value
      });
    }
  },
  watch : {
    '$props.value' : function(val){
      this.setUpdate('form_data',{
        value : val
      });
    },
    'form_data.value' : function(val){
      if(this.$props.input != null){
        this.$props.input(val);
      };
      if(this.$props.inputObject != null){
        this.$props.inputObject({
          [this.$props.name] : val
        })
      }
    }
  },
  render(){
    let {
      label,
      form_title,
      placeholder,
      disabled,
      readonly,
      name,
      type
     } = this.$props;
     let {
      form_data
     } = this.get();
    return (
      <fragment>
      {form_title != null?<h3 class="ui dividing header">{form_title}</h3>:<div class="ui divider"></div>}
      <div class="field">   
        <label for="">{label}</label>
        <div class="ui left icon input">
            <i class="angle right icon"></i> 
            <input type={type} placeholder={placeholder} name={name} disabled={disabled} v-model={form_data.value} readonly={readonly} />
        </div> 
        <div class="base_wr row">
            <span class="base_info error bold small"></span>
        </div>
      </div>
      </fragment>
    )
  }
})

export default InputText;