import BaseVue from 'BaseVue';
import UserStore from '../store/UserStore';
import NavigationStore from '../store/NavigationStore';
import UserService from '../services/UserService';
const Header = BaseVue.extend({
  data : function(){
    return {
      select_category_text : null,
      category_datas : [],
      id : null,
      title : '',
      select_category : null
    }
  },
  methods : {
    onconfig : async function(){
      let self = this;
      NavigationStore.watch((state)=>state.value,async function(val){
        await self.set('title',val.title);
        await self.set('select_category',val.select_category)
        self.setInitDOMSelection('dropdown_select_category',self.get('select_category'));
      })
    },
    oncomplete : async function(){
      let self = this;
      let categories = UserStore.state.categories;
      await self.set('category_datas',categories);
      await self.set('id','header-menu');
      self.parent = $('#'+self.get('id'));
      self.setInitDOMSelection('dropdown_select_category',self.get('select_category'));
      console.log('parent',self.dropdown);
    },
    returnUserService : function(){
      return UserService.create();
    },
    setInitDOMSelection : function(action,props){
      let self = this;
      switch(action){
        case 'dropdown_select_category':
          self.dropdown_select_category = $('#dropdown_select_category').dropdown({
            onChange : function(val,text,$e){
              self.set('select_category_text',text);
              if(val == self.get('select_category')){
                return;
              }
              self.set('select_category',val);
              self.submitSelectCategory();
            }
          })
          if(props == null) return;
          self.dropdown_select_category.dropdown('set selected',props+'');
          break;
      }
    },
    submitSelectCategory : async function(){
      let self = this;
      try{
        let service = this.returnUserService();
        let resData = await service.selectCategory(parseInt(self.get('select_category')));
        window.location.reload();
      }catch(ex){
        console.error('submitSelectCategory - ex ',ex);
      }
    },
    handleClick : function(action,props,e){
      switch(action){
        case 'OPEN_SIDE_MENU':
          $('#wr-sidebar').sidebar('toggle')
        break;
        case 'RIGHT_USER_MENU':
          $('#right-head-pop-menu')
            .popup({
              inline     : true,
              on    : 'click',
              position   : 'bottom right',
              delay: {
                show: 300,
                hide: 800
              }
            })
        break;
      }
    }
  },
  render(){
    const page_name = '';
    const user_data = {};
    const {
      select_category_text,
      category_datas,
      id,
      title
    } = this.get();
    console.log('categories',category_datas);
    return (
      <div class="ui menu" id="header-menu">
        <div class="item burger-menu">
          <a href="base_wr row " href="#">
            <img src="/public/img/menu-icon.svg" alt="" on-click="@this.handleClick('OPEN_SIDE_MENU',{},@event)"/>
          </a>
        </div>
        <div class="item">
            <h5 class="base_info page_name">{title}</h5>
        </div>
        <div class="menus-header">
        </div>
        <div class="main-title mobile hidden">
          <h5 class="base_info page_name">{gettext("Artywiz - Partner")}</h5>
        </div>
        <div class="menus-header">
          <div class="ui dropdown item bar" action="SELECT_CATEGORY" tabindex="0" id="dropdown_select_category">
            {select_category_text||gettext("Select Category")}
            <label></label>
            <i class="dropdown icon"></i> 
            <div class="menu transition hidden" tabindex="-1">
              {category_datas.map((value,index)=>{
                return <a class="item" data-value={value.category.id}>{value.category.name}</a>
              })}
            </div>
          </div>
        </div>
        {/* <div class="ui dropdown" id="list-cuisine">
          <div class="text">List Vendor</div>
          <i class="dropdown icon"></i>
          <div class="menu">
            <div class="item">New</div>
            <div class="item">Latest</div>
          </div>
        </div> */}
        <div class="right item">
          <a class="browse item" id="right-head-pop-menu">
            {user_data.photo != null?
            <img src="{{@this.assetApiUrl('/storage/user/'+user_data.photo)}}" style="height:32px" alt="" onClick={this.handleClick.bind(this,'RIGHT_USER_MENU',{})}/>:
            <img src="/public/img/user-icon.png" alt="" onClick={this.handleClick.bind(this,'RIGHT_USER_MENU',{})}/>}
          </a>
          <div class="ui fluid popup bottom right transition hidden" style="top: auto; left: auto; bottom: 69px; right: 7px;">
            <div class="ui four column relaxed divided grid">
              <div class="column">
                <div class="ui link list">
                  <router-link class="item" to={{name : 'user.profile'}}>
                  {gettext("My Profile")}
                  </router-link>
                  <a class="item" href="/auth/logout">{gettext("Se déconnecter")}</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
})  

export default Header