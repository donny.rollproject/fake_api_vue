import Middleware from '../../../../classes/Middleware.jsx';

module.exports = function(routers){
	routers.push({
		name: 'activity.activity',
		path: '/',
		component: (resolve) => {
			require.ensure([], () => {
				let gg = require('./Activity.jsx');
				resolve(gg);
			});
		},
		beforeEnter: Middleware.bind(this, []),
	})
  routers.push({
		name: 'activity.activity',
		path: '/activity/activity',
		component: (resolve) => {
			require.ensure([], () => {
				let gg = require('./Activity.jsx');
				resolve(gg);
			});
		},
		beforeEnter: Middleware.bind(this, []),
	})
	routers.push({
		name : 'activity.users',
		path: '/activity/users',
		component: (resolve)=>{
			require.ensure([],()=>{
				let gg = require('./Users.jsx');
				resolve(gg);
			})
		}
	})
}