import BaseVue from 'BaseVue';
import BaseComposition from 'BaseComposition';
import ActivityFilter from './ActivityFilter';

const UserFilterBar = ActivityFilter.extend({
  props : {
    inputChangeListener : [null,Function]
  },
  data : function(){
    return {
      query : {}
    }
  },
  methods : {
    onconfig : function(){
      if(this.$props.inputChangeListener != null){
        this.setOnChangeListener(this.$props.inputChangeListener);
      }
    },
    oncomplete : function(){
      let self = this;
      self.$super(UserFilterBar,self).oncomplete();
    },
    setOnChangeListener : function(func){
      let self = this;
      self.onChangeListener = func;
    }
  },
  render(){
    let { query } = this.get();
    return (
      <div class="ui mini menu" id="filter-menu">
        <div class="item nopadding">	
          <div class="ui dropdown item bar" action="TAKE">
            { gettext("Affichage") }&nbsp;<label></label> <i class="dropdown icon"></i>
            <div class="menu">
              <a class="item active selected" data-value="5">{ gettext("5") }</a>
              <a class="item" data-value="20">{ gettext("20") }</a>
              <a class="item" data-value="50">{ gettext("50") }</a>
              <a class="item" data-value="100">{ gettext("100") }</a>
              <a class="item" data-value="200">{ gettext("200") }</a>
            </div>
          </div>
        </div>
        <div class="right item">
          <div class="item" style="display: none">
            <div class="ui icon input">
              <input type="text" name="date_range" data-type="date_range" placeholder="Sélectionner une date"/>
              <i class="search icon"></i>
            </div>
          </div>
          <div class="item" style="display: none">
            <div class="ui action input">
              <input type="text" placeholder="Rechercher" action="SEARCH"/>
              <div class="ui mini button">Ok</div>
            </div>
          </div>
          <div class="ui dropdown item bar" style="display: none" action="ACTION">
            { gettext("Option") }<i class="dropdown icon"></i>
            <div class="menu">
              <a class="item" data-value="reset">{ gettext("Réinitialiser") }</a>
              <a class="item" data-value="export">{ gettext("Télécharger") }</a>
            </div>
          </div>
        </div>
      </div>
    )
  }
})

export default UserFilterBar;