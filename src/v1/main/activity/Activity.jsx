import BaseVue from 'BaseVue';
import Layout from '../Layout';
import Stats from './partials/Stats';
import ActivityFilter from './components/ActivityFilter';
import NavigationStore from '../store/NavigationStore';
import LogService from '../services/LogService';
import SimplePagination from '../components/pagination/SimplePagination';
import SimplePaginationFunction from './functions/SimplePaginationFunction';
import QueryTranslation from './functions/QueryTranslation';
import FilterBarFunction from './functions/FilterBarFunction';

const Activity = BaseVue.extend({
  data : function(){
    return {
      datas : [],
      query : {
        take : 5,
        page : 1,
        total : 0
      }
    }
  },
  components : {
    'stats-bar' : Stats,
    "filter-bar" : ActivityFilter,
    "simple-pagination" : SimplePagination
  },
  watch : {
    '$data.query' : async function(val){
      let self = this;
      self.saveQueryUrl(val);
      self.setLogs(await self.getLogs());
    }
  },
  methods : {
    onconfig : function(){
      let self = this;
      self.FilterBarFunction = FilterBarFunction.create(self);
      self.QueryTranslation = QueryTranslation.create(self);
      self.SimplePaginationFunction = SimplePaginationFunction.create(self);
      self.QueryTranslation.onconfig();
      NavigationStore.commit('SET_VALUE',{
        title : gettext("Activity")
      })
    },
    oncomplete : async function(){
      let self = this;
      self.setInitDOMSelection('filter_bar');
      // self.setLogs(await self.getLogs());
    },
    returnLogService : function(){
      return LogService.create();
    },
    setInitDOMSelection : function(action,props){
      let self = this;
      self.SimplePaginationFunction.setInitDOMSelection(action,props);
      self.FilterBarFunction.setInitDOMSelection(action,props);
    },
    getLogs : async function(){
      let self = this;
      try{
        let service = self.returnLogService();
        let resData = await service.getActivityLogs(self.get('query'));
        return resData;
      }catch(ex){
        console.error('getLogs - ex ',ex);
      }
    },
    setLogs : function(props){
      let self = this;
      if(props == null) return;
      let log_datas = (function(datas){
        return datas;
      })(props.return);
      self.set('datas',log_datas);
      self.setInitDOMSelection('simple_pagination',{
        ...self.get('query'),
        total : log_datas.length,
      });
    },
    handleClick : function(action,props,e){
      let self = this;
      switch(action){
        case 'DETAIL_LOG':
          self.setUpdate('query',{
            search : props.email
          }).then(function(){})
          break;
      }
    }
  },
  render(h){
    let {datas} = this.get();
    return (
      <div id="users" class="base_wr column">
        <stats-bar></stats-bar>
        {this.FilterBarFunction.render(h,{})}
        <table class="ui celled striped padded table">
          <thead>
            <tr>
              <th class="single line">Utilisateur</th>
              <th>N°</th>
              <th>Date</th>
              <th>Type d'info</th>
              <th>Type d'utilisateur</th>
              <th>Resultat</th>
            </tr>
          </thead>
          <tbody>
            {datas.map((value,index)=>{
              return (
                <tr>
                  <td>
                    <a href="#" onClick={this.handleClick.bind(this,'DETAIL_LOG',{id:value.user.id,email:value.user.email})}>{value.user.email}</a>
                  </td>
                  <td class="single line">
                    {value.id}
                  </td>
                  <td>
                    {value.created_at}
                  </td>
                  <td class="right aligned">
                    {value.log_name}
                  </td>
                  <td>
                    {this.safeJSON(value.user,'is_free')==1?gettext('Fremium'):gettext('Preemium')}
                  </td>
                  <td>
                    {value.description}
                  </td>
                </tr>
              )
            })}
          </tbody>
          <tfoot>
            <tr>
              <th colspan="6">
                {this.SimplePaginationFunction.render(h,{})}
              </th>
            </tr>
          </tfoot>
        </table>
      </div>
    )
  }
})

export default Layout.extend({
  data : function(){
    return {
      content : Activity
    }
  }
});