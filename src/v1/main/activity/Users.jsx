import BaseVue from 'BaseVue';
import Layout from '../Layout';
import QueryTranslation from './functions/QueryTranslation';
import NavigationStore from '../store/NavigationStore';
import LogService from '../services/LogService';
import SimplePagination from '../components/pagination/SimplePagination';
import SimplePaginationFunction from './functions/SimplePaginationFunction';
import UserFilterBar from './components/UserFilterBar';
import FilterBarFunction from './functions/FilterBarFunction';

const Users = BaseVue.extend({
  components : {
    "simple-pagination" : SimplePagination,
    "filter-bar" : UserFilterBar
  },
  data : function(){
    return {
      query : {
        take : 20,
        page : 1
      },
      datas : []
    }
  },
  watch : {
    '$data.query' : async function(val){
      let self = this;
      self.saveQueryUrl(val);
      self.setActivityUsers(await self.getActivityUsers());
    }
  },
  methods : {
    onconfig : function(){
      let self = this;
      NavigationStore.commit('SELECT_URL',self.resolveRouteToUrl('activity.activity'));
      NavigationStore.commit('SET_VALUE',{
        title : gettext("Activity Users"),
      })
      self.FilterBarFunction = FilterBarFunction.create(self);
      self.SimplePaginationFunction = SimplePaginationFunction.create(self);
      self.QueryTranslation = QueryTranslation.create(self);
      self.QueryTranslation.onconfig();
    },
    oncomplete : async function(){
      let self = this;
      console.log('vdmfkmvm',self.get('query'))
      self.setActivityUsers(await self.getActivityUsers());
      self.setInitDOMSelection('filter_bar')
    },
    returnLogService : function(){
      return LogService.create();
    },
    setInitDOMSelection : function(action,props){
      let self = this;
      self.SimplePaginationFunction.setInitDOMSelection(action,props);
      self.FilterBarFunction.setInitDOMSelection(action,props);
    },
    getActivityUsers : async function(){
      let self = this;
      let service = self.returnLogService();
      let resData = await service.getActivityUsers(self.get('query'));
      return resData;
    },
    setActivityUsers : function(props){
      let self = this;
      if(props == null) return;
      let datas = (function(datas){
        return datas;
      })(props.return);
      self.set('datas',datas);
      self.setInitDOMSelection('simple_pagination',{
        ...self.get('query'),
        total : datas.length
      })
    }
  },
  render(h){
    let {
      datas,
      query
    } = this.get();
    return (
      <div id="users" class="base_wr column">
        {this.FilterBarFunction.render(h,{})}
        <table class="ui celled striped padded table">
          <thead>
            <tr>
              <th class="single line">No</th>
              <th class="single line">Utilisateur</th>
              <th>N°</th>
              <th>Name</th>
              <th>Updated At</th>
              <th>Option</th>
            </tr>
          </thead>
          <tbody>
            {datas.map((value,index)=>{
              return (
                <tr>
                  <td>
                    {((query.page-1)*query.take)+(index+1)}
                  </td>
                  <td>
                    {value.email}
                  </td>
                  <td class="single line">
                    {value.id}
                  </td>
                  <td>
                    {value.first_name} &nbsp; {value.last_name}
                  </td>
                  <td class="right aligned">
                    {value.updated_at}
                  </td>
                  <td>
                    <i class="ion ion-md-more"></i>
                  </td>
                </tr>
              )
            })}
          </tbody>
          <tfoot>
            <tr>
              <th colspan="6">
                {this.SimplePaginationFunction.render(h,{})}
              </th>
            </tr>
          </tfoot>
        </table>
      </div>
    )
  }
})

export default Layout.extend({
  data : function(){
    return {
      content : Users
    }
  }
});