import BaseVue from 'BaseVue';
import LogService from '../../services/LogService';

const Stats = BaseVue.extend({
  data : function(){
    return {
      stats : {}
    }
  },
  methods : {
    oncomplete : async function(){
      let self = this;
      self.setStats(await self.getStats());
    },
    returnLogService : function(){
      return LogService.create();
    },
    getStats : async function(){
      let self = this;
      try{
        let service = self.returnLogService();
        let resData = await service.getStats();
        return resData;
      }catch(ex){
        console.error('getStats - ex ',ex);
      }
    },
    setStats : function(props){
      let self = this;
      if(props == null) return;
      let stats = (function(datas){
        return datas;
      })(props.return);
      self.set('stats',stats);
    },
    nextUrl : function(){
      return this.$router.resolve({name:'activity.users'}).href
    }
  },
  render(){
    let  {
      stats 
    } = this.get();
    return (
      <div class="ui five column grid">
        <div class="column">
          <div class="ui card">
            <div class="content">
              <div class="logo-wrapper">
                <img width="130" style="margin-bottom: 15px;" src="/public/img/icon-assets/artiwiz-full-width-plain-premium.png" alt=""/>
                <i class="plus circle icon yellow" on-click="@this.handleClick('NEW_USER',{type:'premium'},@event)"></i>
              </div>
              <div class="colored colored--yellow">
                  <div class="center aligned description">
                    <p><a target="_blank" href={this.nextUrl()+"?is_free=false&title="+gettext('Liste des comptes Premium') }><b>{this.safeJSON(stats,'premium')}</b> Comptes</a></p>
                    <p><a target="_blank" href={this.nextUrl()+"?is_free=false&title="+gettext('Liste des comptes Premium') +"&log_name=auth&date=first_time"}><b>{this.safeJSON(stats,'premium_first_time_login')}</b> Comptes activés</a> </p>
                    <p><a target="_blank" href={this.nextUrl()+"?is_free=false&title="+gettext('Liste des comptes Premium') +"&log_name=auth&date=week"}><b>{this.safeJSON(stats,'premium_enabled')}</b> Comptes Actifs</a> </p>
                  </div>
              </div>
            </div>
          </div>
        </div>
        <div class="column">
          <div class="ui card">
            <div class="content">
              <div class="logo-wrapper">
                <img width="130" style="margin-bottom: 15px;" src="/public/img/icon-assets/artiwiz-full-width-plain-freemium.png" alt=""/>
                <i class="plus circle icon blue"  on-click="@this.handleClick('NEW_USER',{type:'freemium'},@event)"></i>
              </div>
              <div class="colored colored--blue">
                  <div class="center aligned description">
                    <p><a target="_blank" href={this.nextUrl()+"?is_free=true&title="+gettext('Liste des comptes Freemium ') }><b>{this.safeJSON(stats,'freemium')}</b> Comptes</a></p>
                    <p><a target="_blank" href={this.nextUrl()+"?is_free=true&title="+gettext('Liste des comptes Freemium ') +"&log_name=auth&date=first_time"}><b>{this.safeJSON(stats,'freemium_first_time_login')}</b> Comptes activés</a></p>
                    <p><a target="_blank" href={this.nextUrl()+"?is_free=true&title="+gettext('Liste des comptes Freemium ') +"&log_name=auth&date=week"}><b>{this.safeJSON(stats,'freemium_enabled')}</b> Comptes Actifs</a></p>
                  </div>
              </div>
            </div>
          </div>
        </div>
        <div class="column">
          <div class="ui card">
            <div class="content">
              <div class="logo-wrapper">
                <img width="130" style="margin-bottom: 15px;" src="/public/img/icon-assets/artywiz-test.png" alt=""/>
                <i class="plus circle icon red"  on-click="@this.handleClick('NEW_USER',{type:'beta'},@event)"></i>
              </div>
              <div class="colored colored--red">
                  <div class="center aligned description">
                    <p><a target="_blank" href={this.nextUrl()+"?is_beta=true&title="+gettext('Liste des comptes Test ') }><b>{this.safeJSON(stats,'beta')}</b> Comptes</a></p>
                    <p><a target="_blank" href={this.nextUrl()+"?is_beta=true&title="+gettext('Liste des comptes Test ') +"&log_name=auth&date=first_time"}><b>{this.safeJSON(stats,'beta_first_time_login')}</b> Comptes activés</a></p>
                    <p><a target="_blank" href={this.nextUrl()+"?is_beta=true&title="+gettext('Liste des comptes Test ') +"&log_name=auth&date=week"}><b>{this.safeJSON(stats,'beta_enabled')}</b> Comptes Actifs</a></p>
                  </div>
              </div>
            </div>
          </div>
        </div>
        <div class="column">
          <div class="ui card">
            <div class="content">
              <div class="logo-wrapper">
                <img width="130" style="margin-bottom: 15px;" src="/public/img/icon-assets/connections.png" alt=""/>
              </div>
              <div class="colored colored--gray">
                  <div class="center aligned description">
                    <p><b>?</b> réussies</p>
                    <p><b>?</b> manquées</p>
                  </div>
              </div>
            </div>
          </div>
        </div>
        <div class="column">
          <div class="ui card">
            <div class="content">
              <div class="logo-wrapper">
                <img width="130" style="margin-bottom: 15px;" src="/public/img/icon-assets/contenus.png" alt=""/>
              </div>
              <div class="colored colored--green">
                  <div class="center aligned description">
                    <p><b>{this.safeJSON(stats,'logo_created')}</b> Logos créés</p>
                    <p><b>{this.safeJSON(stats,'import_logo')}</b> Logos importés</p>
                    <p><b>{this.safeJSON(stats,'mockup_created')}</b> Docs créés</p>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
})

export default Stats;