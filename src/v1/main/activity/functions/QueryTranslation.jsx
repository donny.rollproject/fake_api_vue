import BaseComposition from "../../../../../base/BaseComposition";

export default BaseComposition.extend({
  name : 'QueryTranslation',
  onconfig : function(){
    let self = this.context;
    let parseUrl = self.jsonParseUrl();
    self.setUpdate('query',parseUrl.query);
  }
})