import BaseComposition from "BaseComposition";

export default BaseComposition.extend({
  name : 'SimplePaginationFunction',
  setInitDOMSelection : function(action,props){
    let self = this.context;
    switch(action){
      case 'simple_pagination':
        self.simple_pagination = self.$refs.simplePagination;
        self.simple_pagination.setOnChangeListener(async function(action,props){
          console.log('action',action);
          console.log('props',props);
          switch(action){
            case 'PREV':
              var query = self.get('query');
              if(query.page == 1) return;
              query.page -= 1;
              self.setUpdate('query',query);
              break;
            case 'NEXT':
              var query = self.get('query');
              query.page += 1;
              self.setUpdate('query',query);
              break;
          }
        })
        if(props == null) return;
        window.staticType(props.total,[Number]);
        window.staticType(props.page,[Number]);
        window.staticType(props.take,[Number]);
        self.simple_pagination.setPagination({
          total : props.total,
          page : props.page,
          take : props.take
        })
        break;
    }
  },
  template : function(h){
    return (
      <simple-pagination ref="simplePagination"></simple-pagination>
    )
  }
})