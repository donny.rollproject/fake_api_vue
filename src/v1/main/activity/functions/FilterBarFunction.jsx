import BaseComposition from "BaseComposition";

export default BaseComposition.extend({
  name : 'FilterBarFunction',
  setInitDOMSelection : function(action,props){
    let self = this.context;
    switch(action){
      case 'filter_bar':
        self.filter_bar = self.$refs.filterBar;
        self.filter_bar.setOnChangeListener(function(action,props){
          switch(action){
            case 'SEARCH':
              self.setUpdate('query',{
                search : props
              })
              break;
            case 'ACTION':
              switch(props.value){
                case 'reset':
                return window.location = window.location.href.split("?")[0];
              }
              break;
            case 'DATE':
              self.setUpdate('query',{
                date_start : props.date_start,
                date_end : props.date_end,
              })
              break;
            case 'MULTIPLE_FILTER':
              self.setUpdate('query',{
                multiple_filter : JSON.stringify(props)
              })
              break;
            case 'USER_TYPE':
              self.setUpdate('query',{
                user_type : props.value
              });
              break;
            case 'TAKE':
              self.setUpdate('query',{
                take : props.value
              });
              break;
          }
        })
        break;
    }
  },
  template : function(h){
    return (<filter-bar ref="filterBar"></filter-bar>)
  }
})