import config from '@config';
import BaseService from '../../../base/BaseService';

export default BaseService.extend({
  getApiRoute: async function() {
    try {
      let url = config.API_LIST;
      let resData = await this.getData(url, {
        version: "",
      });
      if (resData.status == "error") throw resData.data.responseJSON;
      return resData;
    } catch (ex) {
      console.error("getApiRoute - ex ", ex);
    }
  },
  setApiRoute: function(route) {
    route = this._super(route);
    window.HTTP_REQUEST = Object.freeze({
      AUTH : {
        REGISTER : route['api.partner.auth.register'],
        LOGIN : route['api.partner.auth.login'],
        LOGOUT : route['api.partner.auth.logout'],
        CATEGORIES : route['api.partner.auth.categories']
      },
      ROUTE: {
        LIST: route["api.partner.routes.list"],
      },
      USER : {
        CURRENT_USER : route['api.partner.user.current_user'],
        UPDATE : route['api.partner.user.update_current_user'],
        SELECT_CATEGORY : route['api.partner.user.select_category']
      },
      CATEGORY : {
        CURRENT_CATEGORIES : route['api.partner.category.current_categories'],
        CATEGORIES : route['api.partner.category.categories']
      },
      ACTIVITY : {
        LOGS : route['api.partner.activity.logs'],
        USERS : route['api.partner.activity.users'],
        STATS : route['api.partner.activity.stats'],
        EXPORT_LOG : route['api.partner.activity.export_logs']
      },
    });
    console.log('HTTP_REQUEST',window.HTTP_REQUEST);
  },
})