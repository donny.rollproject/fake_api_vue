import BaseService from "./BaseService";

export default BaseService.extend({
  getCurrentCategories : async function(){
    try{
      let url = window.HTTP_REQUEST.CATEGORY.CURRENT_CATEGORIES;
      let resData = await this.get(url,{});
      if (resData.status == "error") throw resData.data.responseJSON;
      return resData;
    }catch(ex){
      throw ex;
    }
  }
})