import BaseService from "./BaseService";

export default BaseService.extend({
  getCurrentUser : async function(){
    try{
      let url = window.HTTP_REQUEST.USER.CURRENT_USER;
      let resData = await this.getData(url,{});
      if (resData.status == "error") throw resData.data.responseJSON;
      return resData;
    }catch(ex){
      throw ex;
    }
  },
  updateCurrentUser : async function(props){
    try{
      let url = window.HTTP_REQUEST.USER.UPDATE;
      let form_data = this.objectToFormData(props)
      let resData = await this.postData(url,form_data);
      if (resData.status == "error") throw resData.data.responseJSON;
      return resData;
    }catch(ex){
      throw ex;
    }
  },
  selectCategory : async function(sekectCategoryId){
    window.staticType(sekectCategoryId,[Number]);
    try{
      let url = window.HTTP_REQUEST.USER.SELECT_CATEGORY;
      let form_data = this.objectToFormData({
        default_business_category_id : sekectCategoryId
      })
      let resData = await this.postData(url,form_data);
      if (resData.status == "error") throw resData.data.responseJSON;
      return resData;
    }catch(ex){
      throw ex;
    }
  }
})