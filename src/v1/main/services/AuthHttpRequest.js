import BaseService from "./BaseService";

export default BaseService.extend({
  login: async function(props) {
    window.staticType(props, [Object]);
    try {
      console.log("prop", props);
      let formData = this.objectToFormData(props);
      let resData = await this.postData(window.HTTP_REQUEST.AUTH.LOGIN, formData);
      if (resData.status == "error") throw resData.data.responseJSON;
      return resData;
    } catch (ex) {
      console.error("login - ex ", ex);
      throw ex;
    }
  },
  register: async function(props) {
    window.staticType(props, [Object]);
    try {
      let formData = this.objectToFormData(props);
      let resData = await this.postData(window.HTTP_REQUEST.AUTH.REGISTER, formData);
      if (resData.status == "error") throw resData.data.responseJSON;
      return resData;
    } catch (ex) {
      console.error("register - ex ", ex);
      throw ex;
    }
  },
  logout: async function(){
    try{
      let resData = await this.getData(window.HTTP_REQUEST.AUTH.LOGOUT,{});
      if (resData.status == "error") throw resData.data.responseJSON;
    }catch(ex){
      console.error('logout - ex ',ex);
      throw ex;
    }
  },
  getCategories : async function(parent_id){
    window.staticType(parent_id,[null,Number]);
    try{
      let resData = await this.getData(window.HTTP_REQUEST.AUTH.CATEGORIES,{
        parent_id : parent_id
      })
      if (resData.status == "error") throw resData.data.responseJSON;
      return resData;
    }catch(ex){
      throw ex;
    }
  },
  clearToken : function(){
    window.localStorage.setItem('token',null);
    window.localStorage.setItem('token_type',null);
    window.localStorage.setItem('expired_at',null);
  }
});
