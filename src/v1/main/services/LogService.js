import BaseService from "./BaseService";

export default BaseService.extend({
  getActivityLogs : async function(props){
    window.staticType(props,[Object]);
    try{
     let url = window.HTTP_REQUEST.ACTIVITY.LOGS;
     let resData = await this.getData(url,props);
     if (resData.status == "error") throw resData.data.responseJSON;
     return resData;
    }catch(ex){
      throw ex;
    }
  },
  getStats : async function(){
    try{
      let url = window.HTTP_REQUEST.ACTIVITY.STATS;
      let resData = await this.getData(url,{});
      if (resData.status == "error") throw resData.data.responseJSON;
      return resData;
    }catch(ex){
      throw ex;
    }
  },
  getActivityUsers : async function(props){
    window.staticType(props,[Object]);
    try{
      let url = window.HTTP_REQUEST.ACTIVITY.USERS;
      let resData = await this.getData(url,props);
      if(resData.status == "error") throw resData.data.responseJSON;
      return resData;
    }catch(ex){
      throw ex;
    }
  }
})