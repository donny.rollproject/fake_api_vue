import Vuex from 'vuex';
import Vue from 'vue';

Vue.use(Vuex);

const mutations = {
  UPDATE_LOADING : function(state,val){
    window.staticType(val,[Boolean]);
    state.loading = val;
  },
  SELECT_URL : function(state,val){
    state.select_url = val;
  },
  SET_VALUE : function(state,val){
    state.value = {
      ...state.value,
      ...val
    }
  }
}

const actions = {}
const NavigationStore = new Vuex.Store({
  state : {
    loading : false,
    select_url : null,
    value : {}
  },
  mutations,
  actions
})

var map = [];

for (var key in mutations) {
    map[key] = key;
}
for (var key in actions) {
    map[key] = key;
}

NavigationStore.map = map;
export default NavigationStore;