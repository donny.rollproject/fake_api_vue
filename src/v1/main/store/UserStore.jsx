import Vuex from 'vuex';
import Vue from 'vue';

Vue.use(Vuex);

const mutations = {
  UPDATE_LOADING : function(state,val){
    window.staticType(val,[Boolean]);
    state.loading = val;
  },
  SET_USER : function(state,val){
    window.staticType(val,[Object]);
    state.user = {
      ...state.user,
      ...val
    };
  },
  SET_CATEGORIES : function(state,val){
    window.staticType(val,[Array]);
    state.categories = val;
  },
  CLEAR : function(state){
    state.user = {};
  }
}

const actions = {}
const UserStore = new Vuex.Store({
  state : {
    loading : false,
    user : {},
    categories : []
  },
  mutations,
  actions
})

var map = [];

for (var key in mutations) {
    map[key] = key;
}
for (var key in actions) {
    map[key] = key;
}

UserStore.map = map;
export default UserStore