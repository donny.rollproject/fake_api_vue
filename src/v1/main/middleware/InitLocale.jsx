import config from '@config';
import { addLocale, useLocale } from "ttag";
import 'whatwg-fetch'
let dataPoJSON = null;
const InitLocale  = async (to,from,done,nextMiddleware)=>{
	if(dataPoJSON != null){
		return nextMiddleware();
	}
	let locale = window.theLocale || 'fr';
	try{
		switch(locale){
			case 'en':
				dataPoJSON = await window.fetch(config.ASSET+'/i18n/en.po.json');
			break;
			default:
			case 'fr':
				dataPoJSON = await window.fetch(config.ASSET+'/i18n/fr.po.json');
			break;
		}
		if(dataPoJSON.status == 404){
			return nextMiddleware();
		}
		dataPoJSON = dataPoJSON.data;
		addLocale(locale, dataPoJSON);	
		useLocale(locale);
	}catch(ex){
		console.error('InitLocale - ex',ex);
	}
	nextMiddleware();
}

export default InitLocale;