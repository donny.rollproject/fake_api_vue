import UserStore from "../store/UserStore";
import UserService from "../services/UserService";
import NavigationStore from "../store/NavigationStore";

const InitGetUser  = async (to,from,done,nextMiddleware)=>{
  console.log('to',to);
  console.log('from',from);
  switch(to.name){
    case 'auth.login':
    case 'auth.register':
      return nextMiddleware();
    default:
      break;
  }
  if(Object.keys(UserStore.state.user).length > 0){
		return nextMiddleware();
	}
  try{
    let service = UserService.create();
    let resData = await service.getCurrentUser();
    UserStore.commit('SET_USER',resData.return);
    let categories = resData.return.partner_categories;
    UserStore.commit('SET_CATEGORIES',categories);
    console.log('resData',resData);
    NavigationStore.commit('SET_VALUE',{
      select_category : UserStore.state.user.default_business_category_id
    })
  }catch(ex){
    console.error('InitGetUser - ex ',ex);
    window.router.replace({name : 'auth.login'})
    return;
  }
	nextMiddleware();
}

export default InitGetUser;