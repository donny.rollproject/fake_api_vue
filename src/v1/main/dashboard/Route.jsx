import Middleware from '../../../../classes/Middleware.jsx';

module.exports = function(routers){
  routers.push({
		name: 'dashboard',
		path: '/dashboard',
		component: function(resolve) {
			require.ensure([], () => {
				let gg = require('./Dashboard.jsx');
				resolve(gg);
			});
		},
		beforeEnter: Middleware.bind(this, []),
  })
}