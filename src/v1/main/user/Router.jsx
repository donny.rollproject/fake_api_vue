import Middleware from '../../../../classes/Middleware.jsx';

module.exports = function(routers){
  routers.push({
		name: 'user.profile',
		path: '/user/profile',
		component: (resolve) => {
			require.ensure([], () => {
				let gg = require('./Profile.jsx');
				resolve(gg);
			});
		},
		beforeEnter: Middleware.bind(this, []),
  })
}