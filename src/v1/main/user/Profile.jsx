import BaseVue from 'BaseVue';
import Layout from '../Layout';
import InputText from '../components/input/InputText';
import InputValidation from './functions/InputValidation';
import DisplaySelectCategoryList from './functions/DisplaySelectCategoryList';
import PopupCategory from './partials/PopupCategory';
import NavigationStore from '../store/NavigationStore';
import UserService from '../services/UserService';

const INPUT_TEXT_VALIDATION = {
  id : 'users',
  form_rules : {
    first_name : 'required',
    email : 'required|email',
    select_category_ids : 'required'
  },
  form_attribute_name : {
    first_name : gettext('First Name'),
    email : gettext('Email'),
  },
}

const INPUT_PASSWORD_VALIDATION = {
  id : 'users',
  form_rules : {
    password : 'required|min:8',
    password_confirm : 'required|same:password'
  },
  form_attribute_name : {
    password : gettext('Password'),
    password_confirm : gettext('Password Confirmation'),
  },
}

const SUBMIT_VALIDATION = {
  ...INPUT_TEXT_VALIDATION,
  form_rules : {
    ...INPUT_TEXT_VALIDATION.form_rules,
  },
  form_attribute_name : {
    ...INPUT_TEXT_VALIDATION.form_attribute_name,
  }
}

const Profile = BaseVue.extend({
  components : {
    "input-text" : InputText,
    "popup-category" : PopupCategory
  },
  data : function(){
    return {
      submit_text : gettext("Save"),
      is_email_disabled : false,
      is_email_readonly : true,
      form_data : {},
      form_data_password : {},
      select_category : {}
    }
  },
  methods : {
    onconfig : function(){
      let self = this;
      NavigationStore.commit('SET_VALUE',{
        title : gettext("Profile")
      })
      NavigationStore.commit('SELECT_URL',self.resolveRouteToUrl("user.profile"));
      self.DisplaySelectCategoryList = DisplaySelectCategoryList.create(self);
      self.InputValidation = InputValidation.create(self);
    },
    oncomplete :async function(){
      let self = this;
      self.setInitDOMSelection('input_text_validation',INPUT_TEXT_VALIDATION);
      self.setInitDOMSelection('input_password_validation',INPUT_PASSWORD_VALIDATION);
      self.setInitDOMSelection('popup_category');
      self.setCurrentUser(await self.getCurrentUser());
    },
    returnUserService : function(){
      return UserService.create();
    },
    setInitDOMSelection : function(action,props){
      let self = this;
      switch(action){
        case 'popup_category':
          self.popup_category = self.$refs.popupCategory;
          if(self.popup_category == null) return;
          self.popup_category.setOnChangeListener(async function(action,props){
            window.staticType(props,[Object]);
            window.staticType(props.text,[String]);
            window.staticType(props.id,[Number]);
            switch(action){
              case 'SELECTED':
                await self.setUpdate('select_category',{
                  [props.id] : props.text
                })
                self.popup_category.hide();
                self.validationSelectCategory();
                break;
            }
          })
          break;
      }
      /* Extra function */
      self.InputValidation.setInitDOMSelection(action,props);
    },
    validationSelectCategory : async function(){
      let self = this;
      await self.setUpdate('form_data',{
        select_category_ids : (function(data){
          let datas = [];
          for(var key in data){
            datas.push(key);
          }
          datas = JSON.stringify(datas);
          return datas == '[]'?null:datas;
        })(self.get('select_category'))
      })
      self.setInitDOMSelection('submit_validation',{
        id : SUBMIT_VALIDATION.id,
        form_rules : {
          select_category_ids : 'required'
        }
      })
    },
    handleClick : function(action,props,e){
      let self = this;
      switch(action){
        case 'SUBMIT':
          e.preventDefault();
          var nextValidation = function(){
            self.setInitDOMSelection('submit_validation',{
              ...SUBMIT_VALIDATION,
              callback : function(){
                self.submitData();
              }
            })
          }
          let form_data_password = self.get('form_data_password');
          if(form_data_password.password != null){
            self.setInitDOMSelection('submit_validation',{
              ...INPUT_PASSWORD_VALIDATION,
              form_data : self.get('form_data_password'),
              callback : function(){
                let form_data_password = self.get('form_data_password');
                self.set('form_data',{
                  ...self.get('form_data'),
                  ...self.get('form_data_password')
                })
                nextValidation()
              }
            })
          }else{
            nextValidation();
          }
          
          break;
        case 'BACK':
          e.preventDefault();
          self.$router.go(-1);
          break;
        case 'ADD_CATEGORY':
          e.preventDefault();
          self.popup_category.show();
          break;
      }
      /* Extra function */
      self.DisplaySelectCategoryList.handleClick(action,props,e);
    },
    getCurrentUser : async function(){
      let self = this;
      try{
        let service = self.returnUserService();
        let resData = await service.getCurrentUser();
        return resData;
      }catch(ex){
        console.error('getCurrentUser - ex ',ex);
      }
    },
    setCurrentUser : function(props){
      let self = this;
      if(props == null) return;
      let user = (function(data){
        return data;
      })(props.return);
      self.set('form_data',user);
      let select_category = (function(datas){
        let select_category = {};
        for(var a=0;a<datas.length;a++){
          select_category[datas[a].category_id] = datas[a].category.name;
        }
        return select_category;
      })(user.partner_categories)
      console.log('vdfv',select_category);
      self.set('select_category',select_category);
      self.setUpdate('form_data',{
        select_category_ids : select_category
      })
      self.validationSelectCategory();
    },
    submitData : async function(){
      let self = this;
      try{
        let service = self.returnUserService();
        let form_data = self.get('form_data');
        let resData = await service.updateCurrentUser(form_data);
        window.location.reload();
      }catch(ex){
        console.error('submitdata - ex',ex);
      }
    }
  },
  render(){
    let {
      submit_text,
      is_email_disabled,
      is_email_readonly,
      form_data
    } = this.get();
    return (
      <div id="users" class="base_wr column">
        <form class="base_wr column ui form" id="form-create">
          {/* <input-import label="{{ @global.gettext(`Profil photo`) }}" name="photo" placeholder="{{@global.gettext(`Profil photo`)}}"></input-import> */}
          <input-text label={gettext(`Nom`)} form_title={gettext("User Profile Information")} name="first_name" placeholder={gettext(`Nom`)} value={form_data.first_name} inputObject={(val)=>this.setUpdate('form_data',val)} type="text"></input-text>
          <input-text label={gettext(`Prenom`)} name="last_name" placeholder={gettext(`Prenom`)} type="text" value={form_data.last_name} inputObject={(val)=>this.setUpdate('form_data',val)}></input-text>
          <input-text label={gettext(`Email`)} form_title={gettext(`DÉTAILS`)} name="email" placeholder={gettext(`Email`)} type="email" value={form_data.email} readonly={is_email_readonly} disabled={is_email_disabled}></input-text>
          
          <div class="field">
            <input type="hidden" name="select_category_ids"/>
            <div class="ui left icon input">
              {this.DisplaySelectCategoryList.render(h,{})}
            </div>
            <div class="base_wr row">
                <span class="base_info error bold small"></span>
            </div>
            <button class="ui primary small button" onClick={this.handleClick.bind(this,'ADD_CATEGORY',{})}>{gettext("Select Category (+)")}</button>
          </div>
          <input-text label={gettext(`Créer un mot de passe`)} form_title={gettext(`Créer un mot de passe`)} name="password" placeholder={gettext(`Créer un mot de passe`)} type="password" inputObject={(val)=>this.setUpdate('form_data_password',val)}></input-text>
          <input-text label={gettext(`Recréez un mot de passe`)} name="password_confirm" placeholder={gettext(`Recréez un mot de passe`)} type="password" inputObject={(val)=>this.setUpdate('form_data_password',val)}></input-text>
          {/* <p>{{gettext("Note")}} : --</p> */}
          <p></p>
          <div class="base_wr row btn">
            <button class="ui primary button" onClick={this.handleClick.bind(this,'SUBMIT',{})}>{submit_text}</button>
            <button class="ui basic button" onClick={this.handleClick.bind(this,'BACK',{})}>{gettext("ANNULER")}</button>
          </div>
        </form>
        <popup-category ref="popupCategory"></popup-category>
      </div>
    )
  }
})

export default Layout.extend({
  data : function(){
    return {
      content : Profile
    }
  }
})