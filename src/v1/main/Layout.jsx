import BaseVue from 'BaseVue';
import Header from './components/Header';

const Layout = BaseVue.extend({
  data : function(){
    return {
      header : Header,
      content : null
    }
  },
  render(){
    const HeaderComponent = this.$data.header;
    const ContentComponent = this.$data.content;
    return (
      <fragment>
       <HeaderComponent></HeaderComponent>
       <ContentComponent></ContentComponent>
       </fragment>
    )
  },
})

export default Layout;