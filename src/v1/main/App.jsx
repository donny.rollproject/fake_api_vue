import SideMenu from "./components/SideMenu";
import BaseVue from "BaseVue";
import UserStore from "./store/UserStore";

const App = BaseVue.extend({
  name: "RouteView",
  data: function () {
    return {
			template : null 
		};
  },
  watch: {
    $route: function (to, from) {
      let self = this;
			/* Listen url are changed */
			switch(to.name){
				case 'auth.logout':
				case 'auth.register':
				case 'auth.login':
					self.set('template','auth');
					break;
				default:
					self.set('template','main');
					break;
			}
    }
  },
  methods: {
    onconfig: function () {
      let self = this;
      UserStore.watch(state => state.user, function (val) {
        if (Object.keys(val).length > 0) {
          self.set("is_authenticated", true);
        }
      });
    }
  },
  render(h) {
    let {template} = this.get();
		/* Authentication segment */
		switch(template){
			case 'auth':
				return <router-view></router-view>;
			case 'main':
				return (<div id="main">
					<SideMenu/>
					<div class="pusher">
						<router-view></router-view>
					</div>
				</div>);
			default:
				return null;
		}
  }
});

export default App;