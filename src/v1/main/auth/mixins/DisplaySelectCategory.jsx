export default {
  computed : {
    displayCategories(){
      let { select_category } = this.get();
      window.staticType(select_category,[Object]);   
      const tifOptions = Object.keys(select_category).map(key => 
        <li value={key} style="border:1px solid black;"><div>{select_category[key]}</div> <span onClick={this.handleClick.bind(this,'DELETE_SELECTED_CATEGORY',key)}>X</span></li>
      )
      if(tifOptions.length == 0){
        tifOptions[0] = <li  style="border:1px solid black;">Please Select the category</li>
      }
      return (
        <div>
          <ul>
            {tifOptions}
          </ul>
        </div>
      )
    }
  }
}