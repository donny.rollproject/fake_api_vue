export default {
  data : function(){
    return {
      category_tree_data : {},
      parent_id : null
    }
  },
  methods : {
    getCategories : async function(){
      let self = this;
      try{
        let service = this.returnAuthService();
        let resData = await service.getCategories(self.get('parent_id'));
        return resData;
      }catch(ex){
        console.error('getCategories - ex ',ex);
      }
    },
    setCategories : function(props){
      let self = this;
      if(props == null) return;
      let datas = (function(datas){
        return datas;
      })(props.return);
      let category_tree_data = self.get('category_tree_data');
      let entries = Object.entries(category_tree_data);
      let foundlastIndex = 0;
      for(let [index, [key, value]] of entries.entries()){
        foundlastIndex += 1;
      }
      if(datas.length > 0){
        category_tree_data[foundlastIndex] = datas;
        self.setUpdate('category_tree_data',category_tree_data);
      }
    }
  },
}