import BaseComposition from "BaseComposition";

export default BaseComposition.extend({
  name : 'InputValidation',
  setInitDOMSelection : function(action,props){
    let self = this.context;
    switch(action){
      case 'input_text_validation':
        window.staticType(props,[Object]);
        window.staticType(props.form_attribute_name,[null,Object]);
        window.staticType(props.form_data,[null,Object]);
        window.staticType(props.form_rules,[null,Object]);
        window.staticType(props.element_target,[null,String]);
        window.staticType(props.id,[String]);
        self.inputTextValidation('#'+props.id,{
          form_data : props.form_data || self.get('form_data'),
          form_rules : props.form_rules || self.get('form_rules'),
          form_attribute_name : props.form_attribute_name || self.get('form_attribute_name'),
          element_target : props.element_target || 'input[type=text],input[type=number],input[type=password]'
        },function(res,e){
          let parent = $(e.target).parents('.field').first();
          switch(res.status){
            case 'error':
            return parent.find('span.error').text(res.message);
            case 'valid':
            case 'complete':
            return parent.find('span.error').text('');
          }
        })
        break;
      case 'input_password_validation':
        window.staticType(props,[Object]);
        window.staticType(props.form_attribute_name,[null,Object]);
        window.staticType(props.form_data,[null,Object]);
        window.staticType(props.form_rules,[null,Object]);
        window.staticType(props.id,[String]);
        self.inputTextValidation('#'+props.id,{
          form_data : props.form_data || self.get('form_data'),
          form_rules : props.form_rules || self.get('form_rules'),
          form_attribute_name : props.form_attribute_name || self.get('form_attribute_name'),
          element_target : 'input[type=password]'
        },function(res,e){
          let parent = $(e.target).parents('.field').first();
          switch(res.status){
            case 'error':
            return parent.find('span.error').text(res.message);
            case 'valid':
            case 'complete':
            return parent.find('span.error').text('');
          }
        })
        break;
      case 'submit_validation':
        window.staticType(props,[Object]);
        window.staticType(props.form_attribute_name,[null,Object]);
        window.staticType(props.form_data,[null,Object]);
        window.staticType(props.form_rules,[null,Object]);
        window.staticType(props.id,[String]);
        window.staticType(props.callback,[null,Function]);
        self.submitValidation({
          form_data : props.form_data || self.get('form_data'),
          form_rules : props.form_rules || self.get('form_rules'),
          form_attribute_name : props.form_attribute_name || self.get('form_attribute_name'),
        },function(res){
          console.log('res',res);
          let parent = $('#'+props.id);
          window.eachObject(res.error,function(i,key,val){
            switch(key){
              default:
                var message = parent.find(`input[name=${key}]`).parents('.field').first();
                message.find('span.error').text(val);
                break;
            }
          })
          window.eachObject(res.form_data,function(i,key,vak){
            switch(key){
              default:
                var message = parent.find(`input[name=${key}]`).parents('.field').first();
                message.find('span.error').text('');
                break;
            }
          })
          if(res.status == "complete"){
            if(props.callback == null) return
            props.callback();
          }
        })
        break;
    }
  }
})