import BaseVue from 'BaseVue';
import GetCategory from '../mixins/GetCategory';
import AuthHttpRequest from '../../services/AuthHttpRequest';
import InputDropdown from '../../components/input/InputDropdown';

const PopUpCategory = BaseVue.extend({
  mixins : [GetCategory],
  components : {
    'input-dropdown' : InputDropdown,
  },
  data : function(){
    return {
      id : null,
      select_id : null,
      select_text_category : null
    }
  },
  methods : {
    onconfig : function(){
      let self = this;
      self.set('id',self.getRandomText('ui_modal'));
    },
    oncomplete : function(){
      let self = this;
      self.uiModal = $('#'+self.get('id'));
    },
    returnAuthService : function(){
      return AuthHttpRequest.create();
    },
    show : async function(){
      let self = this;
      self.uiModal.modal('show');
      await self.set('category_tree_data',{});
      await self.set('parent_id',null);
      self.setCategories(await self.getCategories());
    },
    hide : function(){
      let self = this;
      self.uiModal.modal('hide');
    },
    setOnChangeListener : function(func){
      let self = this;
      self.onChangeListener = func;
    },
    handleClick : function(action,props,e){
      let self = this;
      switch(action){
        case 'SELECT_LAST_CATEGORY':
          e.preventDefault();
          self.onChangeListener('SELECTED',{
            text : self.get('select_text_category'),
            id : self.get('select_id')
          })
          break;
      }
    },
    handleDropdownChangeListener : async function(index,props,$e){
      let self = this;
      /* Recondition beginning */
      let category_tree_data = (function(data,stopIndex){
        let new_category = {};
        for(var key in data){
          if(key == stopIndex){
            new_category[key] = data[key];
            break;
          }else{
            new_category[key] = data[key];
          }
        }
        return new_category;
      })(self.get('category_tree_data'),index);
      self.set('category_tree_data',category_tree_data);
      self.set('parent_id',parseInt(props.value));
      self.set('select_id',parseInt(props.value));
      self.set('select_text_category',props.text);
      self.setCategories(await self.getCategories());
    }
  },
  computed : {
    displayDropdown(){
      let {category_tree_data} = this.get();
      let ele = ((data)=>{
        let ele = [];
        let index = 0;
        console.log('before',data["0-key"])
        for(var key in data){
          ele.push(<li value={key}><input-dropdown 
            ref="main_category" 
            datas={data[key]} 
            dropdown_text="name"
            other_value_key="id"
            name={"dropdown-"+key}
            inputChangeListener={this.handleDropdownChangeListener.bind(this,index)}
            ></input-dropdown></li>)
          index+=1;
        }
        return ele;
      })(category_tree_data)
      return(
        <ul>
          {ele}
        </ul>
      )
    }
  },
  render(){
    let { id, select_text_category } = this.get();
    return(
      <div class="ui modal" id={id}>
        <div class="content">
        {this.displayDropdown}
        <button class="ui fluid small teal submit button" onClick={this.handleClick.bind(this,'SELECT_LAST_CATEGORY',{})}>{gettext("Select ")+(select_text_category||'')}</button>
        </div>
      </div>
    )
  }
})

export default PopUpCategory;