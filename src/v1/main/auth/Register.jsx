import BaseVue from 'BaseVue';
import AuthHttpRequest from '../services/AuthHttpRequest';
import GetCategory from './mixins/GetCategory';
import SelectCategory from './mixins/DisplaySelectCategory';
import PopUpCategory from './partials/PopUpCategory';
import InputValidation from './functions/InputValidation';

const INPUT_TEXT_VALIDATION = {
  id : 'form-register',
  form_rules : {
    first_name : 'required',
    last_name : 'required',
    email : 'required|email',
  },
  form_attribute_name : {
    first_name : gettext("First_name"),
    last_name : gettext("Last Name"),
    email : gettext("Email"),
  }
};
const INPUT_PASSWORD_VALIDATION = {
  id : 'form-register',
  form_rules : {
    password : 'required|min:8',
    password_confirm : 'required|same:password',
  },
  form_attribute_name : {
    password : gettext("Password"),
    password_confirm : gettext("Password Confirm"),
  }
}
const SUBMIT_VALIDATION = {
  ...INPUT_TEXT_VALIDATION,
  ...INPUT_PASSWORD_VALIDATION,
  form_rules : {
    ...INPUT_TEXT_VALIDATION.form_rules,
    ...INPUT_PASSWORD_VALIDATION.form_rules,
    select_category_ids : 'required'
  },
  form_attribute_name : {
    ...INPUT_TEXT_VALIDATION.form_attribute_name,
    ...INPUT_PASSWORD_VALIDATION.form_attribute_name,
    select_category_ids : 'Select Category'
  }
}
const Register = BaseVue.extend({
  mixins :[GetCategory,SelectCategory],
  components : {
    "popup-select-category" : PopUpCategory
  },
  data : function(){
    return {
      category_tree_data : {},
      parent_id : null,
      select_category : {},
      form_data : {},
    }
  },
  methods : {
    onconfig : function(){
      let self = this;
      self.InputValidation = InputValidation.create(self);
    },
    oncomplete : async function(){
      let self = this;
      self.setInitDOMSelection('popup_select_category');
      self.setInitDOMSelection('input_text_validation',INPUT_TEXT_VALIDATION);
      self.setInitDOMSelection('input_password_validation',INPUT_PASSWORD_VALIDATION);
    },
    returnAuthService : function(){
      return AuthHttpRequest.create();
    },
    setInitDOMSelection : function(action,props){
      let self = this;
      switch(action){
        case 'popup_select_category':
          self.popup_select_category = self.$refs.popupSelectCategory;
          self.popup_select_category.setOnChangeListener(async function(action,props){
            window.staticType(props,[Object]);
            window.staticType(props.text,[String]);
            window.staticType(props.id,[Number]);
            switch(action){
              case 'SELECTED':
                await self.setUpdate('select_category',{
                  [props.id] : props.text
                })
                await self.setUpdate('form_data',{
                  select_category_ids : self.get('select_category')
                })
                self.popup_select_category.hide();
                self.setInitDOMSelection('submit_validation',{
                  id : SUBMIT_VALIDATION.id,
                  form_rules : {
                    select_category_ids : 'required'
                  }
                })
                break;
            }
          })
          break;
      }
      /* Extra function */
      self.InputValidation.setInitDOMSelection(action,props);
    },
    handleClick : async function(action,props,e){
      let self = this;
      switch(action){
        case 'SUBMIT':
          e.preventDefault();
          self.setInitDOMSelection('submit_validation',{
            ...SUBMIT_VALIDATION,
            callback : function(){
              self.submitData();
            }
          })
          break;
        case 'DELETE_SELECTED_CATEGORY':
          let select_category = self.get('select_category');
          delete select_category[props];
          await self.setUpdate('select_category',select_category);
          break;
        case 'SHOW_POPUP_SELECT_CATEGORY':
          e.preventDefault();
          self.popup_select_category.show();
          break;
      }
    },
    submitData : async function(){
      let self = this;
      try{
        let service = self.returnAuthService();
        let form_data = Object.assign({},self.get('form_data'));
        form_data.select_category_ids = (function(data){
          let select_category_ids = [];
          for(var key in data){
            select_category_ids.push(key);
          }
          return select_category_ids;
        })(form_data.select_category_ids);
        form_data.select_category_ids = JSON.stringify(form_data.select_category_ids);
        let resData = await service.register(form_data);
        swalSuccess(gettext("Success"),gettext("You can login!"),function(){
          self.$router.replace({name : 'auth.login'})
        })
      }catch(ex){
        console.error('submitData - ex ',ex);
      }
    }
  },
  render(){
    let { form_data} = this.get();
    return (
        <div>
          <popup-select-category ref="popupSelectCategory"></popup-select-category>
          <div class="ui middle aligned center aligned grid base_container" id="auth">
            <div class="column">
            
              <img src="/public/img/logo-white-dotted.png" alt="" width="128"/>
              <h2 class="ui image header">
                <div class="content">
                  {gettext("Enregistrer un nouveau compte")} 
                </div>
              </h2>
              
          
              <form action='{{@global.HTTP_REQUEST.AUTH_XHR.REGISTER}}' method="get" id="form-register" class="ui large form">
                <div class="ui stacked secondary  segment">
                  <div class="field">
                    <div class="ui left icon input">
                      <i class="user icon"></i>
                      <input type="text" name="first_name" placeholder={gettext("Prénom")} v-model={form_data.first_name}/>
                    </div>
                    <div class="base_wr row">
                        <span class="base_info error bold small"></span>
                    </div>
                  </div>
                  <div class="field">
                    <div class="ui left icon input">
                      <i class="user icon"></i>
                        <input type="text" name="last_name" placeholder={gettext("Nom de famille")} v-model={form_data.last_name}/>
                    </div>
                    <div class="base_wr row">
                        <span class="base_info error bold small"></span>
                    </div>
                  </div>
                  <div class="field">
                    <div class="ui left icon input">
                      <i class="user icon"></i>
                      <input type="text" name="email" placeholder={gettext("Email")} v-model={form_data.email}/>
                    </div>
                    <div class="base_wr row">
                        <span class="base_info error bold small"></span>
                    </div>
                  </div>
                  <div class="field">
                    <input type="hidden" name="select_category_ids"/>
                    <div class="ui left icon input">
                      {this.displayCategories}
                    </div>
                    <div class="base_wr row">
                        <span class="base_info error bold small"></span>
                    </div>
                    <button class="ui fluid small teal submit button" onClick={this.handleClick.bind(this,'SHOW_POPUP_SELECT_CATEGORY',{})}>{gettext("Select Category (+)")}</button>
                  </div>
                  <div class="field">
                    <div class="ui left icon input">
                      <i class="lock icon"></i>
                      <input type="password" name="password" placeholder={gettext("Mot de passe")} v-model={form_data.password}/>
                    </div>
                    <div class="base_wr row">
                        <span class="base_info error bold small"></span>
                    </div>
                  </div>
                  <div class="field">
                    <div class="ui left icon input">
                      <i class="lock icon"></i>
                      <input type="password" name="password_confirm" placeholder={gettext("Confirmation mot de passe")} v-model={form_data.password_confirm}/>
                    </div>
                    <div class="base_wr row">
                        <span class="base_info error bold small"></span>
                    </div>
                  </div>
                  <button class="ui fluid large teal submit button" onClick={this.handleClick.bind(this,'SUBMIT',{})}>{gettext("S'inscrire")}</button>
                </div>
                <div class="ui error message"></div>
              </form>
              <div class="ui message">
                {gettext("Avoir un utilisateur?")} <router-link to={{name:'auth.login'}}>{gettext("Retour connexion")}</router-link>
              </div>
            </div>
          </div>
        </div>
        
    )
  }
})
export default Register;