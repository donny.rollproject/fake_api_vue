import Login from "./Login"
import UserStore from "../store/UserStore";

const Logout = Login.extend({
  methods : {
    onconfig : function(){
      UserStore.commit('CLEAR')
    },
    oncomplete : async function(){
      try{
        if(window.localStorage.getItem('token') == null){
          window.location.href = this.$router.resolve({name:'auth.login'}).href;
          return;
        }
        let service = this.returnAuthService();
        let resData = await service.logout();
        service.clearToken();
      }catch(ex){
        console.error('oncomplete - ex ',ex);
      }
    }
  },
  render(){
    return (
      <div class="ui middle aligned center aligned grid base_container" id="auth">
        <div class="column">
          <h2 class="ui image header">
            <div class="content">
              {gettext("Vous êtes déconnecté!")}
            </div>
          </h2>
          <div class="ui message">
            <router-link to={{name:'auth.login'}}>{gettext("Retour connexion")}</router-link>
          </div>
        </div>
      </div>
    )
  }
});
export default Logout;