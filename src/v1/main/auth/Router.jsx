import Middleware from '../../../../classes/Middleware.jsx';
var initAuth = async (to,from,done,nextMiddleware)=>{
	require.ensure([],function(){
		require("@asset/v1/css/backend_auth.scss");
		nextMiddleware();
	})
}
module.exports = function(routers){
  routers.push({
		name: 'auth.login',
		path: '/auth/login',
		component: (resolve) => {
			require.ensure([], () => {
				let gg = require('./Login.jsx');
				resolve(gg);
			});
		},
		beforeEnter: Middleware.bind(this, [initAuth]),
  })

  routers.push({
		name: 'auth.logout',
		path: '/auth/logout',
		component: (resolve) => {
			require.ensure([], () => {
				let gg = require('./Logout.jsx');
				resolve(gg);
			});
		},
		beforeEnter: Middleware.bind(this, [initAuth]),
  })

  routers.push({
		name: 'auth.register',
		path: '/auth/register',
		component: (resolve) => {
			require.ensure([], () => {
				let gg = require('./Register.jsx');
				resolve(gg);
			});
		},
		beforeEnter: Middleware.bind(this, [initAuth]),
  })
}