import BaseVue from 'BaseVue';
import AuthHttpRequest from '../services/AuthHttpRequest';
import InputValidation from './functions/InputValidation';
const INPUT_TEXT_VALIDATION = {
  id : 'form-login',
  form_rules : {
    email : 'required|email',
    password : 'required'
  },
  form_attribute_name : {
    email : gettext("Email"),
    password : gettext("Mot de passe")
  }
}
const SUBMIT_VALIDATION = {
  ...INPUT_TEXT_VALIDATION,
}
const Login = BaseVue.extend({
  data : function(){
    return {
      notification : [],
      form_data : {},
      
    }
  },
  methods : {
    onconfig : function(){
      let self = this;
      self.InputValidation = InputValidation.create(self);
    },
    oncomplete : function(){
      let self = this;
      self.$super(Login,this).oncomplete();
      self.setInitDOMSelection('input_text_validation',INPUT_TEXT_VALIDATION);
    },
    returnAuthService : function(){
      return AuthHttpRequest.create();
    },
    submit : async function(){
      let self = this;
      try{
        let service = self.returnAuthService();
        let resData = await service.login(self.get('form_data'));
        let token = resData.return;
        window.localStorage.setItem('token',token.access_token);
        window.localStorage.setItem('token_type',token.token_type);
        window.localStorage.setItem('expires_at',token.expires_at);
        window.location.href = '/';
      }catch(ex){
        console.error('submit - ex ',ex);
        let message = self.parseException(ex.return.message);
        self.set('notification',Array.isArray(message)?message:[message]);
      }
    },
    handleClick : function(action,props,e){
      let self = this;
      switch(action){
        case 'SUBMIT':
          e.preventDefault();
          self.setInitDOMSelection('submit_validation',{
            ...SUBMIT_VALIDATION,
            callback : function(){
              self.submit()
            }
          })
          break;
      }
    },
    setInitDOMSelection : function(action,props){
      let self = this;
      /* Extra Function */
      self.InputValidation.setInitDOMSelection(action,props);
    }
  },
  render(){
    let notification = this.get('notification');
    let form_data = this.get('form_data');
    return (
      <div class="ui middle aligned center aligned grid base_container" id="auth">
        <div class="column">
          <img src="/public/img/logo-white-dotted.png" alt="" width="128"/>
          <h1 class="ui image header">
              {gettext("Connectez-vous à votre compte")}
          </h1>
          {notification != null?
            <div class="base_wr column notification">
            {notification.map((value, index) => {
              return <h5>{value}</h5>
            })}
          </div> : null}
          <form action="" id="form-login" method="get" class="ui large form">
            <div class="ui stacked secondary  segment">
              <div class="field">
                <div class="ui left icon input">
                  <i class="user icon"></i>
                  <input type="text" name="email" placeholder={gettext('Email')} autocomplete="off" v-model={form_data.email}/>
                </div>
                <div class="base_wr row">
                  <span class="base_info error bold small"></span>
                </div>
              </div>
              <div class="field">
                <div class="ui left icon input">
                  <i class="lock icon"></i>
                  <input type="password" name="password" placeholder={gettext('Mot de passe')} autocomplete="off" v-model={form_data.password}/>
                </div>
                <div class="base_wr row">
                  <span class="base_info error bold small"></span>
                </div>
              </div>
              <button class="ui fluid large teal submit button" onClick={this.handleClick.bind(this,'SUBMIT',{})}>{gettext("S'identifier")}</button>
            </div>
            <div class="ui error message"></div>
          </form>
          <div class="ui message">
            {gettext("Nouveau pour nous?")} <router-link to={{name:'auth.register'}}>{gettext("S'inscrire")}</router-link>
          </div>
        </div>
      </div>
    )
  }
})

export default Login;