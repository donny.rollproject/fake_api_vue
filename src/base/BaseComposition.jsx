import UberProto from 'uberproto';

export default UberProto.extend({
  __init : 'construct',
  construct : function(context){
    this.context = context;
  },
  render : function(h,props){
    var template = this.template.bind(this.context);
    return template(h,props);
  },
  getStackTrace : function () {
    let stack = new Error().stack || '';
    stack = stack.split('\n').map(function (line) { return line.trim(); });
    return stack.splice(stack[0] == 'Error' ? 2 : 1);
  },
  set : function(...props){
    let self = this;
    let strk = this.getStackTrace();
    let str = strk[1];
    str = str.split('.')[1].split('(')[0];
    str = str.replace(/\s/g, '');
    if(self[str] != null){
      return self[str](...props);
    }
    console.warn('Donny!. You dont have function with this name!!, Check your composition!!');
    return null;
  }
})